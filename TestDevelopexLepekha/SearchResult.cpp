#include "SearchResult.h"

SearchResult::SearchResult(int id, QString url, QObject *parent)
    : QObject(parent),
      _id(id),
      _url(url)
{
}

SearchResult &SearchResult::operator=(const SearchResult &other)
{
    if (this != &other)
    {
        _id = other.getId();
        _url = other.url();
        setIsFound(other.isFound());
        updateStatus(other.status());
        setErrorMessage(other.errorMessage());
    }
    return *this;
}

SearchResult::SearchStatus SearchResult::status() const
{
    return _status;
}

QString SearchResult::url() const
{
    return _url;
}

const QStringList &SearchResult::childUrls() const
{
    return _childUrls;
}

bool SearchResult::isFound() const
{
    return _isFound;
}

QString SearchResult::errorMessage() const
{
    return _errorMessage;
}

int SearchResult::getId() const
{
    return _id;
}

void SearchResult::updateStatus(SearchResult::SearchStatus status)
{
    if (_status == status)
        return;

    _status = status;
    emit statusChanged();
}

void SearchResult::setChildUrls(QStringList &childs)
{
    _childUrls = childs;
}

void SearchResult::setIsFound(bool isFound)
{
    if (isFound == _isFound)
        return;

    _isFound = isFound;
    emit isFoundChanged();
}

void SearchResult::appendChildUrl(QString urlStr)
{
    _childUrls.append(urlStr);
}

void SearchResult::setErrorMessage(QString errorMessage)
{
    if (_errorMessage == errorMessage)
        return;

    _errorMessage = errorMessage;
    emit errorMessageChanged();
}
