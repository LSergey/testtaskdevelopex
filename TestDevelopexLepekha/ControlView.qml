import QtQuick 2.0
import QtQuick.Controls 2.2
import SearchComponent 1.0

Rectangle {
    id: _parent
    color: "#dddddd"
    width: parent.width
    height: 200
    property double lineWidth: parent.width / 2.2
    property int fontPointSize: 16
    property string textColor: "#e6b01b"
    property string fontFamily: "Helvetica"

    Row {
        y: 10
        spacing: 20
        anchors.horizontalCenter: parent.horizontalCenter

        Button {
            id: _startBtn
            text: "Start"
            enabled: SearchFacade.canStart
            background: Rectangle {
                implicitWidth: 110
                implicitHeight: 40
                opacity: enabled ? 1 : 0.3
                border.color: _startBtn.down ? "#FA8072" : "#696969"
                border.width: 1
                radius: 4
            }
            onClicked: {
                SearchFacade.settings.url = _urlTextEdit.getText()
                SearchFacade.settings.maxThreadsNum = _threadsNumberTextEdit.getText()
                SearchFacade.settings.searchText = _searchTextEdit.getText()
                SearchFacade.settings.maxUrlsNum = _maxUrlsTextEdit.getText()
                SearchFacade.start()
            }
        }

        Button {
            id: _pauseBtn
            text: SearchFacade.isPaused ? "Continue" : "Pause"
            enabled: SearchFacade.searchRunning
            background: Rectangle {
                implicitWidth: 110
                implicitHeight: 40
                opacity: enabled ? 1 : 0.3
                border.color: _pauseBtn.down ? "#FA8072" : "#696969"
                border.width: 1
                radius: 4
            }
            onClicked: {
                SearchFacade.isPaused ? SearchFacade.continueSearch() : SearchFacade.pause()
            }
        }

        Button {
            id: _stopBtn
            text: "Stop"
            enabled: SearchFacade.searchRunning
            background: Rectangle {
                implicitWidth: 110
                implicitHeight: 40
                opacity: enabled ? 1 : 0.3
                border.color: _stopBtn.down ? "#FA8072" : "#696969"
                border.width: 1
                radius: 4
            }
            onClicked: {
                SearchFacade.stop()
            }
        }
    }

    Column {
        id: _settings
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 40
        y: 90

        Row {
            id: firstRow
            spacing: 20

            EditTextControl {
                id: _urlTextEdit
                width: lineWidth
                height: 30
                descriptionText: "Initial url"
                text: "https://google.com"
            }

            EditTextControl {
                id: _threadsNumberTextEdit
                width: lineWidth
                height: 30
                descriptionText: "Threads number"
                text: "1"
            }
        }

        Row {
            id: secondRow
            spacing: 20

            EditTextControl {
                id: _searchTextEdit
                width: lineWidth
                height: 30
                descriptionText: "Search string"
                text: "Facebook"
            }

            EditTextControl {
                id: _maxUrlsTextEdit
                width: lineWidth
                height: 30
                descriptionText: "Max urls to parse"
                text: "10"
            }
        }
    }
}
