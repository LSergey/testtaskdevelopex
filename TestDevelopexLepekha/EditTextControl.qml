import QtQuick 2.0

Item {
    id: _parent
    property int fontPointSize: 16
    property int descriptionFontPointSize: 12
    property string textColor: "#f27640"
    property string descriptionColor: "#f24940"
    property string fontFamily: "Helvetica"
    property string text: ""
    property string descriptionText: ""

    Rectangle {
        color: "#ffffff"
        height: _textEdit.cursorRectangle.height
        width: _parent.width
    }

    Text {
        id: _description
        y: -_textEdit.cursorRectangle.height
        anchors.horizontalCenter: _parent.horizontalCenter
        font.family: fontFamily
        font.pointSize: descriptionFontPointSize
        color: descriptionColor
        text: parent.descriptionText
        elide: Text.ElideRight
    }

    TextEdit {
        id: _textEdit
        anchors.fill: _parent
        text: _parent.text
        font.family: fontFamily
        font.pointSize: fontPointSize
        color: textColor
     }

    function getText() {
        return _textEdit.text;
    }
}
