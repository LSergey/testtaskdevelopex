import QtQuick 2.0
import QtQuick.Controls 2.2
import SearchComponent 1.0
import "."

Item {
    id: _parent

    Rectangle {
        color: "#b4b4b4"
        height: _parent.height
        width: _parent.width
    }

    ListView {
        id: _listView
        width: _parent.width
        height: _parent.height
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 5
        model: SearchFacade.resultModel
        clip: true
        delegate: ResultViewDelegate {
            result: model.display
        }

        ScrollBar.vertical: ScrollBar {}
     }
}
