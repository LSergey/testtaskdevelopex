#include "SearchManager.h"
#include "SearchWorker.h"
#include <QThread>

SearchManager::SearchManager(QObject *parent)
    : QObject(parent)
{
}

void SearchManager::setSearchSettings(std::shared_ptr<SearchSettings>& settings)
{
    _searchSettings = settings;
}

void SearchManager::setResultModel(std::shared_ptr<SearchResultModel>& resultModel)
{
    _resultModel = resultModel;
}

void SearchManager::start()
{
    resetControlValues();
    _inProgress = true;
    _urlsQueue.append(_searchSettings->url());

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(100);

    emit searchStateChanged();
}

void SearchManager::pause()
{
    _isPause = true;
    emit searchStateChanged();
}

void SearchManager::stop()
{
    resetControlValues();
    _isStop = true;

    if (_runningThreadsCount == 0)
        _inProgress = false;
    emit searchStateChanged();
}

void SearchManager::continueSearch()
{
    _isPause = false;
    emit searchStateChanged();
}

bool SearchManager::canBeStarted() const
{
    return (_inProgress == false) && (_isPause == false);
}

bool SearchManager::isPaused() const
{
    return _isPause;
}

bool SearchManager::isRunning() const
{
    return _inProgress;
}

void SearchManager::searchResultUpdated(SearchResult *result)
{
    if (_resultModel->rowCount() <= result->getId())
        return;

    _resultModel->updateData(result);
}

void SearchManager::workerFinishResult(SearchResult *result)
{
    searchResultUpdated(result);

    if ( result->childUrls().size() > 0 && !(_isPause || _isStop))
    {
        for (int i = 0; i < result->childUrls().size(); ++i)
        {
            QString childUrl = result->childUrls().at(i);
            if (!_proccessedList.contains(childUrl)
                    && !_urlsQueue.contains(childUrl))
                _urlsQueue.append(childUrl);
        }
    }

    if (result->status() == SearchResult::SearchStatus::Finished)
        QObject::sender()->deleteLater();
}

void SearchManager::update()
{
    if (_isPause || _isStop)
        return;

    if (_runningThreadsCount < _searchSettings->maxThreadsNum() &&
            _urlsQueue.size() > 0 && _idCounter < _searchSettings->maxUrlsNum().toInt())
    {
        QString urlStr = _urlsQueue.at(0);
        _proccessedList.append(urlStr);
        _urlsQueue.pop_front();
        runThread(_idCounter, urlStr);
        ++_idCounter;
    }
}

void SearchManager::runThread(int id, QString urlStr)
{
    //qDebug() << "run new thread " << id << urlStr;
    QThread* thread = new QThread();
    auto searchResult = std::make_shared<SearchResult>(id, urlStr);
    _resultModel->insertNewItem(searchResult);

    SearchWorker* worker = new SearchWorker(id, urlStr, _searchSettings->searchText());
    worker->moveToThread(thread);

    connect(worker, SIGNAL(searchResultUpdated(SearchResult*)),
            this, SLOT(searchResultUpdated(SearchResult*)));
    connect(worker, SIGNAL(workFinished(SearchResult*)),
            this, SLOT(workerFinishResult(SearchResult*)));
    connect(thread, SIGNAL (started()), worker, SLOT (startWork()));
    connect(worker, SIGNAL (workFinished(SearchResult*)), thread, SLOT (quit()));
    connect(thread, SIGNAL (finished()), thread, SLOT (deleteLater()));
    connect(thread, &QThread::finished, this, [this]() {
        --_runningThreadsCount;
        if (_runningThreadsCount == 0 && (_urlsQueue.size() == 0 || _idCounter >= _searchSettings->maxUrlsNum().toInt()))
        {
            _inProgress = false;
            emit searchStateChanged();
        }
    });
    thread->start();
    _runningThreadsCount++;
}

void SearchManager::resetControlValues()
{
    _isStop = false;
    _isPause = false;
    _idCounter = 0;
    _urlsQueue.clear();
    _proccessedList.clear();
}
