import QtQuick 2.9
import QtQuick.Window 2.2
import "."

Window {
    id: _parent
    visible: true
    width: 800
    height: 1000
    title: qsTr("Test task")
    minimumWidth: 600
    minimumHeight: 400


    ControlView {
        id: _controlArea
        anchors.horizontalCenter: parent.horizontalCenter
    }

    ResultsView {
        id: _resultView
        anchors.horizontalCenter: parent.horizontalCenter
        y: 200
        width: _parent.width
        height: _parent.height - _resultView.y
    }
}
