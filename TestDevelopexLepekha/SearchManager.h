#pragma once

#include <QObject>
#include <memory>
#include "SearchSettings.h"
#include "qmlSearchResultModel.h"

class SearchManager: public QObject
{
    Q_OBJECT
public:
    explicit SearchManager(QObject *parent = nullptr);

    void setSearchSettings(std::shared_ptr<SearchSettings>& settings);
    void setResultModel(std::shared_ptr<SearchResultModel>& resultModel);

    void start();
    void pause();
    void stop();
    void continueSearch();

    bool canBeStarted() const;
    bool isPaused() const;
    bool isRunning() const;

signals:
    void searchStateChanged();

public slots:
    void searchResultUpdated(SearchResult* result);
    void workerFinishResult(SearchResult* result);
    void update();

private:
    bool _isStop = false;
    bool _isPause = false;
    bool _inProgress = false;
    int _idCounter = 0;
    int _runningThreadsCount = 0;
    std::shared_ptr<SearchSettings> _searchSettings;
    std::shared_ptr<SearchResultModel> _resultModel;
    QString _url;
    QStringList _urlsQueue;
    QStringList _proccessedList;

    void runThread(int id, QString urlStr);
    void resetControlValues();
};
