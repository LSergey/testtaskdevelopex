#include "SearchWorker.h"
#include <QStringList>

SearchWorker::SearchWorker(int id, QString urlStr, QString searchText, QObject *parent)
    : QObject(parent),
      _id(id),
      _urlStr(urlStr),
      _searchText(searchText)
{
}

SearchWorker::~SearchWorker()
{
}

void SearchWorker::startWork()
{
    _networkManager = new QNetworkAccessManager(this);
    _networkManager->setRedirectPolicy(QNetworkRequest::RedirectPolicy::NoLessSafeRedirectPolicy);
    connect(_networkManager, SIGNAL(finished(QNetworkReply*)),
            SLOT(downloadFinished(QNetworkReply*)));

    _result = new SearchResult(_id, _urlStr, this);
    _result->updateStatus(SearchResult::SearchStatus::Loading);
    emit searchResultUpdated(_result);

    QNetworkRequest request(_urlStr);
    request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    _networkManager->get(request);
}

void SearchWorker::downloadFinished(QNetworkReply *reply)
{
    if (reply->error()) {
        _result->updateStatus(SearchResult::SearchStatus::Loading_error);
        QString errorMessage;
        if (!reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString().isEmpty())
            errorMessage = "Error code: " +
                reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString();
        else
            errorMessage = "No error code available";

        _result->setErrorMessage(errorMessage);
        emit workFinished(_result);
    }
    else
    {
        _result->updateStatus(SearchResult::SearchStatus::Search);
        emit searchResultUpdated(_result);
        _pageText = QString(reply->readAll());
        searchText();
    }

    reply->deleteLater();
}

void SearchWorker::searchText()
{
    bool isFound = false;
    if (_pageText.indexOf(_searchText) != -1)
        isFound = true;

    _result->setIsFound(isFound);
    _result->updateStatus(SearchResult::SearchStatus::GettingUrls);
    emit searchResultUpdated(_result);

    getChildUrls();
}

void SearchWorker::getChildUrls()
{
    QRegExp regExp("(https?|ftp|file):\/\/[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
    int pos = 0;
    while ((pos = regExp.indexIn(_pageText, pos)) != -1) {
        QString link = regExp.cap();
        _result->appendChildUrl(link);
        pos += regExp.matchedLength();
    }

    _result->updateStatus(SearchResult::SearchStatus::Finished);
    emit workFinished(_result);
}
