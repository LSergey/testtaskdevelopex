#include <QApplication>
#include <QQmlApplicationEngine>
#include "SearchSettings.h"
#include "qmlSearchFacade.h"
#include "qmlSearchResultModel.h"
#include "SearchResult.h"

void registerQmlTypes()
{
    qmlRegisterSingletonType<SearchFacade>("SearchComponent", 1, 0, "SearchFacade",
                                           SearchFacade::searchFacadeSingletontypeProvider);
    qmlRegisterType<SearchSettings>("SearchComponent", 1, 0, "SearchSettings");
    qmlRegisterType<SearchResult>("SearchComponent", 1, 0, "SearchResult");
    qmlRegisterType<SearchResultModel>("SearchComponent", 1, 0, "SearchResultModel");
}

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QApplication app(argc, argv);

    registerQmlTypes();

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
