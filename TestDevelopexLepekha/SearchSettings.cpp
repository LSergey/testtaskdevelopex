#include "SearchSettings.h"

SearchSettings::SearchSettings(QObject *parent)
    : QObject(parent)
{
}

QString SearchSettings::url() const
{
    return _url;
}

QString SearchSettings::searchText() const
{
    return _searchText;
}

QString SearchSettings::maxThreadsNum() const
{
    return _maxThreadsNum;
}

QString SearchSettings::maxUrlsNum() const
{
    return _maxUrlsNum;
}

void SearchSettings::setUrl(QString url)
{
    _url = url;
}

void SearchSettings::setSearchText(QString searchText)
{
    _searchText = searchText;
}

void SearchSettings::setMaxThreadsNum(QString maxThreadsNum)
{
    _maxThreadsNum = maxThreadsNum;
}

void SearchSettings::setMaxUrlsNum(QString maxUrlsNum)
{
    _maxUrlsNum = maxUrlsNum;
}
