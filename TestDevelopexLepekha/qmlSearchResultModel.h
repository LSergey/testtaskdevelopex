#pragma once

#include <QAbstractListModel>
#include <vector>
#include <memory>
#include "SearchResult.h"

class SearchResultModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit SearchResultModel(QObject* parent = nullptr);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role) const override;

    void updateData(SearchResult* result);
    void insertNewItem(std::shared_ptr<SearchResult>& newResult);

    void reset();

private:
    std::vector< std::shared_ptr<SearchResult> > _resultsVec;
};
