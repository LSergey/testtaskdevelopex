#pragma once

#include <QObject>
#include <QQmlApplicationEngine>
#include <memory>
#include "qmlSearchResultModel.h"
#include "SearchSettings.h"
#include "SearchManager.h"

class SearchFacade: public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool canStart READ canStart NOTIFY canStartChanged)
    Q_PROPERTY(bool isPaused READ isPaused NOTIFY isPausedChanged)
    Q_PROPERTY(bool searchRunning READ searchRunning NOTIFY searchRunningChanged)
    Q_PROPERTY(SearchResultModel* resultModel READ resultModel NOTIFY resultModelChanged)
    Q_PROPERTY(SearchSettings* settings READ settings NOTIFY settingsChanged)

public:
    static QObject* searchFacadeSingletontypeProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
    explicit SearchFacade(QObject *parent = nullptr);

    Q_INVOKABLE void start();
    Q_INVOKABLE void pause();
    Q_INVOKABLE void stop();
    Q_INVOKABLE void continueSearch();

    SearchResultModel* resultModel() const;
    SearchSettings* settings() const;
    bool canStart() const;
    bool isPaused() const;
    bool searchRunning() const;

signals:
    void resultModelChanged();
    void settingsChanged();
    void canStartChanged();
    void isPausedChanged();
    void searchRunningChanged();

public slots:
    void searchManagerStatusHandler();

private:
    std::unique_ptr<SearchManager> _searchManager;
    std::shared_ptr<SearchResultModel> _resultModel;
    std::shared_ptr<SearchSettings> _settings;
};
