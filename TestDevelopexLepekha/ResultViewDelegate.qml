import QtQuick 2.0
import SearchComponent 1.0

Item {
    id: _parent
    width: parent.width
    height: 40
    property int spacing: 20
    property SearchResult result

    Rectangle {
        color: result.getId() % 2 ? "#ececec" : "#e1dfdc"
        height: _parent.height
        width: _parent.width
    }

    Text {
        id: _url
        x: 0
        width: _parent.width * 0.6
        height: _parent.height
        text: result.url
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    Text {
        id: _status
        x: _url.width + _parent.spacing
        width: _parent.width * 0.15
        height: _parent.height
        verticalAlignment: Text.AlignVCenter
        text: getStatusText(result.status)
        elide: Text.ElideRight
    }

    Text {
        id: _isFound
        width: _parent.width * 0.25
        height: _parent.height
        anchors.left: _status.right
        verticalAlignment: Text.AlignVCenter
        visible: result.status == 4
        text: result.isFound ? "Founded" : "Not Found"
        elide: Text.ElideRight
    }
    Text {
        id: _error
        width: _parent.width * 0.25
        height: _parent.height
        anchors.left: _status.right
        verticalAlignment: Text.AlignVCenter
        visible: result.status == 5
        text: result.errorMessage
        elide: Text.ElideRight
    }


    function getStatusText(statusNumber) {
        var resultText;

        switch(statusNumber) {
        case 0:
            resultText = "In queue";
            break;
        case 1:
            resultText = "Url loading";
            break;
        case 2:
            resultText = "Searching text";
            break;
        case 3:
            resultText = "Extract child urls";
            break;
        case 4:
            resultText = "Finished";
            break;
        case 5:
            resultText = "Loading error";
            break;
        }

        return resultText;
    }
}
