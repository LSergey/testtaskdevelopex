#pragma once

#include <QObject>
#include <QtNetwork>
#include "SearchResult.h"

class SearchWorker: public QObject
{
    Q_OBJECT
public:
    explicit SearchWorker(int id, QString urlStr, QString searchText, QObject *parent = nullptr);
    ~SearchWorker();

signals:
    void searchResultUpdated(SearchResult*);
    void workFinished(SearchResult*);

public slots:
    void startWork();
    void downloadFinished(QNetworkReply* reply);

private:
    int _id;
    QString _searchText;
    QString _urlStr;
    QString _pageText;
    SearchResult* _result;
    QNetworkAccessManager* _networkManager;

    void searchText();
    void getChildUrls();
};
