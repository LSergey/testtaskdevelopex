#pragma once

#include <QObject>
#include <QString>

class SearchSettings : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString url READ url WRITE setUrl)
    Q_PROPERTY(QString searchText READ searchText WRITE setSearchText)
    Q_PROPERTY(QString maxThreadsNum READ maxThreadsNum WRITE setMaxThreadsNum)
    Q_PROPERTY(QString maxUrlsNum READ maxUrlsNum WRITE setMaxUrlsNum)
public:
    explicit SearchSettings(QObject *parent = nullptr);

    QString url() const;
    QString searchText() const;
    QString maxThreadsNum() const;
    QString maxUrlsNum() const;

    void setUrl(QString url);
    void setSearchText(QString searchText);
    void setMaxThreadsNum(QString);
    void setMaxUrlsNum(QString maxUrlsNum);

private:
    QString _url;
    QString _searchText;
    QString _maxThreadsNum;
    QString _maxUrlsNum;
};
