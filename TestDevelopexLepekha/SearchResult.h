#pragma once

#include <QObject>
#include <QString>

class SearchResult : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString url READ url NOTIFY urlChanged)
    Q_PROPERTY(SearchStatus status READ status NOTIFY statusChanged)
    Q_PROPERTY(bool isFound READ isFound NOTIFY isFoundChanged)
    Q_PROPERTY(QString errorMessage READ errorMessage NOTIFY errorMessageChanged)

public:
    explicit SearchResult(int id = -1, QString url = "", QObject *parent = nullptr);

    SearchResult& operator=(const SearchResult& other);

    enum SearchStatus {
        Queue,
        Loading,
        Search,
        GettingUrls,
        Finished,
        Loading_error
    };
    Q_ENUM(SearchStatus)

    Q_INVOKABLE int getId() const;

    bool isFound() const;
    SearchStatus status() const;
    QString url() const;
    QString errorMessage() const;
    const QStringList &childUrls() const;

    void updateStatus(SearchStatus status);
    void setChildUrls(QStringList &childs);
    void setIsFound(bool isFound);
    void appendChildUrl(QString urlStr);
    void setErrorMessage(QString errorMessage);

signals:
    void statusChanged();
    void urlChanged();
    void isFoundChanged();
    void errorMessageChanged();

private:
    bool _isFound;
    int _id;
    SearchStatus _status = SearchStatus::Queue;
    QString _url;
    QString _errorMessage;
    QStringList _childUrls;
};
