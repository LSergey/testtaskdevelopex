#include "qmlSearchFacade.h"

QObject *SearchFacade::searchFacadeSingletontypeProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    SearchFacade *searchFacade = new SearchFacade();
    return searchFacade;
}

SearchFacade::SearchFacade(QObject *parent)
    : QObject(parent)
{
    _searchManager = std::make_unique<SearchManager>(this);
    _resultModel = std::make_shared<SearchResultModel>(this);
    _settings = std::make_shared<SearchSettings>(this);

    connect(_searchManager.get(), &SearchManager::searchStateChanged, this, &SearchFacade::searchManagerStatusHandler);
}

SearchResultModel *SearchFacade::resultModel() const
{
    return _resultModel.get();
}

SearchSettings *SearchFacade::settings() const
{
    return _settings.get();
}

void SearchFacade::start()
{
    _resultModel->reset();
    _searchManager->setSearchSettings(_settings);
    _searchManager->setResultModel(_resultModel);
    _searchManager->start();
}

void SearchFacade::pause()
{
    _searchManager->pause();
}

void SearchFacade::stop()
{
    _searchManager->stop();
}

void SearchFacade::continueSearch()
{
    _searchManager->continueSearch();
}

void SearchFacade::searchManagerStatusHandler()
{
    emit canStartChanged();
    emit isPausedChanged();
    emit searchRunningChanged();
}

bool SearchFacade::canStart() const
{
    return _searchManager->canBeStarted();
}

bool SearchFacade::isPaused() const
{
    return _searchManager->isPaused();
}

bool SearchFacade::searchRunning() const
{
    _searchManager->isRunning();
}
