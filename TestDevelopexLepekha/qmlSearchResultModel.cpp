#include "qmlSearchResultModel.h"

SearchResultModel::SearchResultModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int SearchResultModel::rowCount(const QModelIndex &parent) const
{
    return _resultsVec.size();
}

QVariant SearchResultModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= _resultsVec.size())
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole)
        return QVariant::fromValue(_resultsVec[index.row()].get());
    else
        return QVariant();
}

void SearchResultModel::updateData(SearchResult* result)
{
    auto modelIndex = this->index(result->getId());
    *(_resultsVec[result->getId()].get()) = *result;
    emit dataChanged(modelIndex, modelIndex, {Qt::EditRole, Qt::DisplayRole});
}

void SearchResultModel::insertNewItem(std::shared_ptr<SearchResult>& newResult)
{
    beginInsertRows(QModelIndex(), newResult->getId(), newResult->getId());
    _resultsVec.push_back(newResult);
    endInsertRows();
}

void SearchResultModel::reset()
{
    beginResetModel();
    _resultsVec.clear();
    endResetModel();
}
